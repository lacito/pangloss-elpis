import os
from pathlib import Path
import ruamel.yaml as yaml

global configuration

def configure(configuration_path="src/configuration.yml"):
    with open(configuration_path, "r") as file:
        data = yaml.safe_load(file)
    return data

configuration = configure()

def remove_empty_folders(directory, preserve=False):
    for root, folders, _ in os.walk(directory, topdown=False):
        for folder in folders:
            try:
                path = Path(root) / folder
                os.removedirs(path)
                print(f"Removed {path} empty path.")
            except OSError:
                pass

def verify_prior_path(folder, name, default_path):
    prior_path = list(folder.rglob(Path(name).as_posix()))
    assert len(prior_path) <= 1, f"Several files with same name ({name}), it must not happen ({prior_path})!"
    return prior_path[0] if prior_path else default_path

def load_resources(resource_path):
    with open(resource_path, "r") as file:
        resources = yaml.YAML(typ="safe").load(file)
        print(f"Resources loaded from {resource_path}.")
    return resources

def save_resources(resources, resource_path):
    resource_path.parent.mkdir(parents=True, exist_ok=True)
    print(resource_path)
    with open(resource_path, "wb") as file:
        yaml.YAML().dump(resources, file)
        print(f"YML resources saved in {resource_path}.")
