from pathlib import Path

from tools import verify_prior_path, remove_empty_folders

def arrange_files(resources, target_path=Path(), criterias=[]):
    size = len(resources)
    for index, resource_data in enumerate(resources.values(), 1):
        print(f"""Resource to arrange {index}/{size}: "{resource_data["name"]}":""")
        for data_type in ["annotation", "audio"]:
            criteria_path = get_criteria_path(resource_data, criterias)
            final_path = target_path / Path(criteria_path) / resource_data[data_type]["original file"]
            current_path = Path(resource_data[data_type]["original path"])
            if not current_path.exists():
                potential_path = verify_prior_path(target_path, resource_data[data_type]["original file"], None)
                if potential_path:
                    print(f"{current_path} not found, but {potential_path} found and will be used!")
                    current_path = potential_path
                else:
                    print(f"{current_path} not found, nor any file of same name in {target_path} folder, ignored!")
            if current_path.exists() and current_path != final_path:
                final_path.parent.mkdir(parents=True, exist_ok=True)
                current_path.rename(final_path)
                resource_data[data_type]["original path"] = final_path.as_posix()
                print(f"File {current_path} moved to {final_path}.")
    remove_empty_folders(target_path)
    return resources

def get_criteria_path(resource, criterias):
    bad_criterias = [criteria for criteria in criterias if not resource.get(criteria)]
    if bad_criterias:
        criterias = [criteria for criteria in criterias if criteria not in bad_criterias]
        print(f"""Some criterias are not valid ({", ".join(bad_criterias)}) and will be ignored!""")
    if criterias:
        criteria_path = Path(*[" ; ".join(resource[criteria]) if isinstance(resource[criteria], list) else resource[criteria] for criteria in criterias])
    else:
        criteria_path = Path()
    return criteria_path
