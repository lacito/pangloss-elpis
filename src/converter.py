from pathlib import Path
import re
import string
import subprocess
from sys import platform

from tools import configuration, verify_prior_path

def convert_annotation_files(resources, language, target_path, source_format, target_format, xsl_arguments, force=False, limit=None):
    style_path = Path("src/templates") / f"{source_format}→{target_format}.xsl"
    if not style_path.exists() or target_format not in configuration["converter"]:
        raise Exception(f"Unknown format ({target_format})!")
    else:
        size = len(resources)
        for index, resource_data in enumerate(resources.values(), 1):
            print(f"""Resource to convert {index}/{size}: "{resource_data["name"]}":""")
            source_path = resource_data["annotation"]["original path"]
            target_template = string.Template(configuration["converter"][target_format]["filename template"])
            target_name = target_template.substitute(name=resource_data["name"])
            target_path = verify_prior_path(target_path, target_name, Path(source_path).parent / target_name)
            data = {
                "version": xsl_arguments["eaf_version"],
                "author": xsl_arguments.get("authors", resource_data["authors"]),
                "participant": xsl_arguments.get("speakers", resource_data["speakers"]),
                "language": language,
                "language code": xsl_arguments.get("language_code", resource_data["language code"]),
                "audio path": resource_data["audio"]["original file"],
                "translation language codes": xsl_arguments["translation_language_codes"],
                "bad character regex": xsl_arguments["bad_char_regex"]}
            convert_file(source_path, target_path, style_path, data, force)
            resource_data["annotation"][f"converted {target_format} path"] = target_path.as_posix()
            if limit and index == limit:
                break
        return resources

def convert_file(source_path, target_path, style_path, data, force):
    if not force and target_path.exists():
        print(f"\t{target_path} already converted!")
    else:
        parameters = " ".join([f'%s="{value}"' % re.sub(r"\s", "_", key) for key, value in data.items() if isinstance(value, str)])
        if platform in ["linux", "linux2"]:
            xslt_command = "saxonb-xslt"
        elif platform == "darwin":
            xslt_command = "saxon"
        command = f"{xslt_command} -s:'{source_path}' -xsl:'{style_path}' -o:'{target_path}' {parameters}"
        print(f"""XSLT command (in "{str(target_path.parent)}" folder):\n{command}""")
        try:
            subprocess.run(command, shell=True)
        except FileNotFoundError as exception:
            print(f"Please get an XSLT 2 compiler, like saxonb. ({exception})")
