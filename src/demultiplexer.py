import librosa
from pathlib import Path
from shutil import copy
import soundfile
import string

from tools import configuration, verify_prior_path

def demultiplex_files(resources, target_path, annotations="original", force=False, limit=None):
    target_template = string.Template(configuration["demultiplexer"]["filename template"])
    size = len(resources)
    for index, resource_data in enumerate(resources.values(), 1):
        print(f"""Resource to demultiplex {index}/{size}: "{resource_data["name"]}":""")
        audio_path = Path(resource_data["audio"]["original path"])
        demultiplexed_audio_paths = demultiplex_audio_file(audio_path, target_path, target_template, force)
        resource_data["audio"]["demultiplexed paths"] = []
        resource_data["audio"][f"duplicated {annotations} paths"] = []
        for demultiplexed_audio_path in demultiplexed_audio_paths:
            resource_data["audio"]["demultiplexed paths"].append(demultiplexed_audio_path.as_posix())
            annotation_path = Path(resource_data["annotation"]["original path"]) if annotations == "original" else Path(resource_data["annotation"][f"converted {annotations} path"])
            duplicated_annotation_path = demultiplexed_audio_path.with_suffix(annotation_path.suffix)
            duplicate_annotation_file(annotation_path, duplicated_annotation_path, force)
            resource_data["audio"][f"duplicated {annotations} paths"].append(duplicated_annotation_path.as_posix())
        if limit and index == limit:
            break
    return resources

def demultiplex_audio_file(source_path, target_path, template, force):
    audio_content = librosa.load(source_path, sr=None, mono=False)
    channel_number = len(audio_content[0].shape)
    channels = [audio_content[0]] if channel_number == 1 else audio_content[0]
    target_paths = []
    for channel, audio_data in enumerate(channels, 1):
        target_name = Path(template.substitute(name=source_path.stem, channel=channel, extension=source_path.suffix))
        target_path = verify_prior_path(target_path, target_name, source_path.parent / target_name)
        if not force and target_path.exists():
            print(f"\t{target_path} already demultiplexed!")
        else:
            soundfile.write(target_path, audio_data, audio_content[1], 'PCM_16')
        target_paths.append(target_path)
    return target_paths

def duplicate_annotation_file(source_path, target_path, force):
    if not force and target_path.exists():
        print(f"\t{target_path} already duplicated!")
    else:
        copy(source_path, target_path)
