
from pprint import pprint
import ruamel.yaml as yaml
import sickle

from tools import configuration, load_resources

def get_resources(language, target_path, force=False):
    if not force and target_path.exists() and target_path.stat().st_size:
        resources = load_resources(target_path)
    else:
        resources = download_resources(language)
    return resources

def download_resources(language):
    namespaces = {"dc": "http://purl.org/dc/elements/1.1/", "olac": "http://www.language-archives.org/OLAC/1.1/"}
    language_records = harvest_oai(language)
    assert language_records, f"""There are no files associated to "{language}" language name, please check that the language name matches the Pangloss metadata!"""
    annotation_records = {identifier: record for identifier, record in language_records.items() if "text/xml" in record.metadata["format"]}
    audio_records = {identifier: record for identifier, record in language_records.items() if "audio/x-wav" in record.metadata["format"]}
    records = {identifier: record for identifier, record in language_records.items()}
    pprint({a: b.metadata for a, b in audio_records.items()}, open("temp/audio.txt", "w"))
    pprint({a: b.metadata for a, b in annotation_records.items()}, open("temp/annotation.txt", "w"))
    pprint({a: b.metadata for a, b in records.items()}, open("temp/all.txt", "w"))
    resources = {annotation_identifier: {
        "audio": {
            "identifier": audio_records[annotation_record.metadata.get("requires", [])[-1]].header.identifier,
            "date": audio_records[annotation_record.metadata.get("requires", [])[-1]].header.datestamp,
            "record": audio_records[annotation_record.metadata.get("requires", [])[-1]].raw,
            "url": list(filter(lambda file:file.endswith(".wav"), audio_records[annotation_record.metadata.get("requires", [])[-1]].metadata["isFormatOf"]))[0],
            },
        "annotation": {
            "identifier": annotation_identifier,
            "date": annotation_record.header.datestamp,
            "record": annotation_record.raw,
            "url": list(filter(lambda file: file.endswith(".xml"), annotation_record.metadata["identifier"]))[0],
            },
        "language code": str(annotation_record.xml.xpath("*//dc:subject/@olac:code", namespaces=namespaces)[0]),
        "authors": [speaker.text for speaker in annotation_record.xml.xpath("*//dc:contributor[@olac:code='researcher' and (not(@xml:lang) or @xml:lang = 'en')]", namespaces=namespaces)],
        "speakers": [speaker.text for speaker in audio_records[annotation_record.metadata.get("requires", [])[-1]].xml.xpath("*//dc:contributor[@olac:code='speaker' and (not(@xml:lang) or @xml:lang = 'en')]", namespaces=namespaces)],
        } for annotation_identifier, annotation_record in annotation_records.items() if len(annotation_record.metadata.get("requires", [])) >= 1 and annotation_record.metadata.get("requires", [])[-1] in audio_records}  # Last required audio of the list seems to be the most up-to-date.
    invalid_audio_resources = {annotation_identifier: annotation_record.metadata.get("requires", []) for annotation_identifier, annotation_record in annotation_records.items() if len(annotation_record.metadata.get("requires", [])) != 1}
    missing_audio_resources = {annotation_identifier: annotation_record.metadata.get("requires", [])[-1] for annotation_identifier, annotation_record in annotation_records.items() if len(annotation_record.metadata.get("requires", [])) == 1 and annotation_record.metadata.get("requires", [])[-1] not in audio_records}
    print("Invalid audio resources:")
    pprint(invalid_audio_resources)
    print("Missing audio resources:")
    pprint(missing_audio_resources)
    return resources

def harvest_oai(language, server=configuration["retriever"]["server"], metadataPrefix="olac", set_identifier=configuration["retriever"]["set identifier"]):
    data = sickle.Sickle(server)
    print(f"Harvesting oai resources from {server} (set identifier: {set_identifier}, metadata prefix: {metadataPrefix}), it can take several minutes…")
    records = data.ListRecords(metadataPrefix=metadataPrefix, set=set_identifier)
    language_records = {record.header.identifier: record for record in records if language in record.metadata.get("subject", [])}
    print(f"Resources for {language}: {len(language_records)} files")
    return language_records
