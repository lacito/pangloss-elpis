#! python3

import argparse
from pathlib import Path

import tools
import harvester
import downloader
import arranger
import converter
import demultiplexer

default_language_path = Path("data/languages/")

parser = argparse.ArgumentParser(prog="Pangloss tools.")
subparsers = parser.add_subparsers(dest="command", help="sub-command help")
harvester_parser = subparsers.add_parser("harvest", aliases=["hv"], help="Harvest help.")
downloader_parser = subparsers.add_parser("download", aliases=["dl"], help="Download help.")
arranger_parser = subparsers.add_parser("arranger", aliases=["ar"], help="Arranger help.")
converter_parser = subparsers.add_parser("convert", aliases=["cv"], help="Convert help.")
demultiplexer_parser = subparsers.add_parser("demultiplex", aliases=["dm"], help="Demultiplex help.")

harvester_parser.add_argument("language", type=str, help="Language information to harvest from Pangloss.")
harvester_parser.add_argument("--target-path", type=Path, default=default_language_path, help="""Resource file path the harvested data will be stored in (for faster further downloads and conversions). (Default: "data/languages/$language/resources.yml")""")
harvester_parser.add_argument("--force", type=bool, nargs="?", const=True, default=False, help="Harvest oai resources even if the last resource used already exists in temp folder.  (Default: False)")

downloader_parser.add_argument("language", type=str, help="Language data to download from Pangloss.")
downloader_parser.add_argument("--resource-path", type=Path, default=default_language_path, help="""Resource path for harvested data. (Default: "data/languages/$language/resources.yml")""")
downloader_parser.add_argument("--target-path", type=Path, default=default_language_path, help="""Target folder the data will be stored in. (Default: "data/languages/$language/")""")
downloader_parser.add_argument("--force", type=bool, nargs="?", const=True, default=False, help="Download a file even if the target already exists – with same name. (Default: False)")
downloader_parser.add_argument("--limit", type=int, help="This feature limits the amount of data that will be downloaded. It's useful in case you are simply carrying out tests. (Default: no limit)")

arranger_parser.add_argument("language", type=str, help="Language data to download from Pangloss.")
arranger_parser.add_argument("--resource-path", type=Path, default=default_language_path, help="""Resource path for harvested data. (Default: "data/languages/$language/resources.yml")""")
arranger_parser.add_argument("--target-path", type=Path, default=default_language_path, help="""Target folder the data will be stored in. (Default: "data/languages/$language/")""")
arranger_parser.add_argument("--criterias", type=str, nargs="?", action="append", default=[], const=["type", "speakers"], help="""The criterias for structure sorting of downloaded files. (Default: [type, speakers], as "--criterias type --criterias speakers")""")

converter_parser.add_argument("language", type=str, help="Language data to convert.")
converter_parser.add_argument("--resource-path", type=Path, default=default_language_path, help="""Resource path for harvested data. (Default: "data/languages/$language/resources.yml")""")
converter_parser.add_argument("--target-path", type=Path, default=default_language_path, help="""Target folder the data will be write in. (Default: "data/languages/$language/converted")""")
converter_parser.add_argument("--force", type=bool, nargs="?", const=True, default=False, help="Convert a file even if the target already exists – with same name. (Default: False)")
converter_parser.add_argument("--limit", type=int, help="This feature limits the amount of data that will be converted. It's useful in case you are simply carrying out tests. (Default: no limit)")
converter_parser.add_argument("--source-format", type=str, default="pangloss", help="The source format. (Default: pangloss)")
converter_parser.add_argument("--target-format", type=str, default="elan", help="The target format. (Default: elan)")
converter_parser.add_argument("--xsl-eaf-version", type=str, default="2.7", help="The version for XSL file.")
converter_parser.add_argument("--xsl-authors", type=str, help="The authors for XSL file.")
converter_parser.add_argument("--xsl-speakers", type=str, help="The speakers for XSL file.")
converter_parser.add_argument("--xsl-language-code", type=str, help="The language code for XSL file.")
converter_parser.add_argument("--xsl-translation-language-codes", type=str, help="The translation language codes for XSL file.")
converter_parser.add_argument("--xsl-bad-char-regex", type=str, default="", help="The regex removing bad characters for XSL file.")

demultiplexer_parser.add_argument("language", type=str, help="Language data to generate.")
demultiplexer_parser.add_argument("--resource-path", type=Path, default=default_language_path, help="""Resource path for harvested data. (Default: "data/languages/$language/resources.yml")""")
demultiplexer_parser.add_argument("--target-path", type=Path, default=default_language_path, help="""Target folder the data will be write in. (Default: "data/languages/$language/generated")""")
demultiplexer_parser.add_argument("--annotations", type=str, default="original", help="""Specify what annotations to duplicate (original annotations, or any supported converted format) according to demultiplexed files. (Default: "original")""")
demultiplexer_parser.add_argument("--force", type=bool, nargs="?", const=True, default=False, help="Generate a file even if the target already exists – with same name. (Default: False)")
demultiplexer_parser.add_argument("--limit", type=int, help="This feature limits the amount of data that will be demultiplexed. It's useful in case you are simply carrying out tests. (Default: no limit)")

def complete_arguments(arguments):
    abbreviations = {"hv": "harvest", "dl": "download", "ar": "arrange", "cv": "convert", "dm": "demultiplex"}
    arguments.command = abbreviations.get(arguments.command, arguments.command)
    if arguments.command == "harvest":
        if arguments.target_path == default_language_path:
            arguments.target_path = arguments.target_path / arguments.language / "resources.yml"
    else:
        if arguments.resource_path == default_language_path:
            arguments.resource_path = arguments.resource_path / arguments.language / "resources.yml"
        if arguments.target_path == default_language_path:
            arguments.target_path = arguments.target_path / arguments.language
    return arguments

def dispatch_commands(arguments):
    if arguments.command == "harvest":
        resources = harvester.get_resources(arguments.language, arguments.target_path, arguments.force)
        tools.save_resources(resources, arguments.target_path)
    elif arguments.command == "download":
        resources = harvester.get_resources(arguments.language, arguments.resource_path)
        resources = downloader.get_files(resources, arguments.target_path, arguments.force, arguments.limit)
        tools.save_resources(resources, arguments.resource_path)
    elif arguments.command == "arrange":
        resources = harvester.get_resources(arguments.language, arguments.resource_path)
        resources = arranger.arrange_files(resources, arguments.target_path, arguments.criterias)
        tools.save_resources(resources, arguments.resource_path)
    elif arguments.command == "convert":
        resources = harvester.get_resources(arguments.language, arguments.resource_path)
        xsl_arguments = {key.replace("xsl_", ""): value for key, value in arguments._get_kwargs() if "xsl_" in key}
        resources = converter.convert_annotation_files(resources, arguments.language, arguments.target_path, arguments.source_format, arguments.target_format, xsl_arguments, arguments.force, arguments.limit)
        tools.save_resources(resources, arguments.resource_path)
    elif arguments.command == "demultiplex":
        resources = harvester.get_resources(arguments.language, arguments.resource_path)
        resources = demultiplexer.demultiplex_files(resources, arguments.target_path, arguments.annotations, arguments.force, arguments.limit)
        tools.save_resources(resources, arguments.resource_path)


if __name__ == "__main__":
    arguments = complete_arguments(parser.parse_args())
    argument_expression = "\n".join([f"\t{key} = {value}"for (key, value) in arguments._get_kwargs()])
    print(f"Arguments:\n{argument_expression}")
    process = dispatch_commands(arguments)
