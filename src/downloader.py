
import backoff
import datetime
import http
import os
from pathlib import Path
import string
import urllib.request
from xml.etree import ElementTree

from tools import configuration, verify_prior_path

def get_files(resources, target_path, force=False, limit=None):
    target_path.mkdir(parents=True, exist_ok=True)
    size = len(resources)
    for index, (annotation_identifier, resource_data) in enumerate(resources.items(), 1):
        resource_data["name"] = Path(resource_data["annotation"]["url"]).stem
        print(f"""Resource to download {index}/{size}: "{resource_data["name"]}":""")
        for data_type in ["annotation", "audio"]:
            print(f"""{data_type} id: {resource_data[data_type]["identifier"]}""")
            template = string.Template(configuration["downloader"][data_type]["filename template"])
            name = template.substitute(name=resource_data["name"])
            path = verify_prior_path(target_path, name, target_path / name)
            content = get_file(resource_data[data_type]["url"], path, resource_data[data_type]["date"], force)
            if data_type == "annotation":
                resource_data["type"] = ElementTree.fromstring(content).tag.lower()
                if not resource_data["speakers"]:
                    print(f"Be careful, resource {annotation_identifier} does not have any speaker information…")
            resource_data[data_type]["original file"] = path.name
            resource_data[data_type]["original path"] = path.as_posix()
        if limit and index == limit:
            break
    return resources

@backoff.on_exception(backoff.expo, (urllib.error.HTTPError, urllib.error.URLError, http.client.IncompleteRead))
def get_file(link, target_path, update_date, force):
    update_date = datetime.datetime.strptime(update_date, '%Y-%m-%d')
    if (not target_path.exists()) or (target_path.stat().st_mtime != update_date.timestamp()) or force:
        print(f"\t{target_path} (link: {link}) will be downloaded.")
        start = datetime.datetime.now()
        with urllib.request.urlopen(link) as response:
            raw_data = response.read()
        speed = ((len(raw_data)/1024**2)/(datetime.datetime.now() - start).total_seconds())
        print(f"Downloaded at speed of {speed} MB/s.")
        with open(target_path, "wb") as file:
            file.write(raw_data)
        os.utime(target_path, (datetime.datetime.now().timestamp(), update_date.timestamp()))
    else:
        print(f"\t{target_path} (link: {link}) already downloaded and up-to-date.")
        with open(target_path, "rb") as file:
            raw_data = file.read()
    return raw_data
