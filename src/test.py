from datetime import datetime
import urllib.request

# Même fichier.
lien = "https://cocoon.huma-num.fr/data/archi/masters/234737.wav"
for compteur in range(1, 11):
    réponse = urllib.request.urlopen(lien)
    début = datetime.now()
    données = réponse.read()
    vitesse = ((len(données)/1024**2)/(datetime.now() - début).total_seconds())
    print(f"Essai {compteur} avec débit de {vitesse} Mo/s")

# Différents fichiers.
liens = [
    "https://cocoon.huma-num.fr/data/archi/masters/235213.wav",
    "https://cocoon.huma-num.fr/data/archi/masters/235228.wav",
    "https://cocoon.huma-num.fr/data/archi/masters/235231.wav",
    "https://cocoon.huma-num.fr/data/archi/masters/234730.wav",
    "https://cocoon.huma-num.fr/data/archi/masters/234731.wav",
    "https://cocoon.huma-num.fr/data/archi/masters/234732.wav",
    "https://cocoon.huma-num.fr/data/archi/masters/234733.wav",
    "https://cocoon.huma-num.fr/data/archi/masters/234734.wav",
    "https://cocoon.huma-num.fr/data/archi/masters/234736.wav",
    "https://cocoon.huma-num.fr/data/archi/masters/234737.wav"]
for (compteur, lien) in enumerate(liens, 1):
    réponse = urllib.request.urlopen(lien)
    début = datetime.now()
    données = réponse.read()
    vitesse = ((len(données)/1024**2)/(datetime.now() - début).total_seconds())
    print(f"Essai {compteur} avec débit de {vitesse} Mo/s")
