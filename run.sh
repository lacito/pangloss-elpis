./src/run.py harvest "Yongning Na"
./src/run.py download "Yongning Na"
./src/run.py arrange "Yongning Na" --criterias type --criterias speakers
./src/run.py convert "Yongning Na" --xsl-translation-language-codes "fr zh" --xsl-bad-char-regex "[◊|]"
./src/run.py demultiplex "Yongning Na" --target-path "../Language sets/Yongning Na"
./src/run.py cv "Yongning Na" --xsl-translation-language-codes "fr zh" --xsl-bad-char-regex "[◊|]" --target-format pangloss_transcriptions
